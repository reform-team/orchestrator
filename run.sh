#!/bin/sh

set -eux

if [ -e ./log ]; then
	if ! tail -1 log | grep SUCCESS; then
		echo "last run was not successful... skipping to preserve log" >&2
		exit 0
	fi
fi

exec 1> ./log 2>&1

: "${WWWDATA:=/var/www}"

SIGNKEY=
if [ "$#" -eq "1" ]; then
	# reform.d.n key: F83356BBE112B7462A41552F7D5D8C60CF4D3EB4
	SIGNKEY="$1"
fi

set -- fakechroot fakeroot devscripts mmdebstrap reprepro sbuild debhelper debian-keyring quilt pristine-tar rsync git python3-debian faketime python3-jinja2 uidmap mmdebstrap genext2fs ca-certificates e2fsprogs git mount parted python3-dacite bmap-tools
if [ "$(dpkg-query --showformat '${db:Status-Status}\n' --show "$@" | sort -u)" != "installed" ]; then
	echo "Not all dependencies of this script are installed."
	echo "Run the following command to install them:"
	echo
	echo "    sudo apt install " "$@"
	exit 1
fi

# If we are in a git repository and if SOURCE_DATE_EPOCH is not set or set but
# null, use the timestamp of the latest git commit. Otherwise, use the provided
# value (if not null) or default to the timestamp of now.
if [ -z ${SOURCE_DATE_EPOCH:+x} ] && git -C . rev-parse 2>/dev/null; then
	SOURCE_DATE_EPOCH=$(git log -1 --format=%ct)
else
	: "${SOURCE_DATE_EPOCH:=$(date +%s)}"
fi
export SOURCE_DATE_EPOCH

# Setup repository

if [ ! -e ~/.sbuildrc ]; then
	# shellcheck disable=SC2016 # Intentional quoting technique
	echo '$chroot_mode = '"'unshare';" > ~/.sbuildrc
elif ! perl -n0e 'eval($_); $chroot_mode eq "unshare" ? exit 0 : exit 1' ~/.sbuildrc; then
	echo "you already have an ~/.sbuildrc set up but without \$chroot_mode = 'unshare'"
	exit 1
fi
mkdir -p ~/.cache/sbuild/
mmdebstrap --variant=buildd bookworm ~/.cache/sbuild/bookworm-arm64.tar

# to build linux for backports we need to enable the backports archive and
# allow automatic installation of kernel-wedge
# shellcheck disable=SC2016 # Intentional quoting technique
mmdebstrap --variant=buildd \
	--setup-hook='echo deb http://deb.debian.org/debian bookworm-backports main > "$1/etc/apt/sources.list.d/backports.list"' \
	--setup-hook='printf "Package: kernel-wedge\nPin: release n=bookworm-backports\nPin-Priority: 900\n" > "$1/etc/apt/preferences.d/99kernel-wedge.pref"' \
	bookworm ~/.cache/sbuild/bookworm-backports-arm64.tar

rm -rf reform-debian-packages/changes
# Not removing the repository or otherwise src:linux will be rebuilt every day.
# This is usually not a problem (other than the waste of electricity) because
# src:linux builds reproducibly but it will cause problems once the build
# environment changes and then a package will be produced with the same version
# but different checksums than the one before.
# rm -rf reform-debian-packages/repo

env --chdir=reform-debian-packages BASESUITE=bookworm OURSUITE=bookworm OURLABEL=reform.debian.net ./setup.sh
cat << END > "reform-debian-packages/repo/conf/distributions"
Codename: bookworm
Label: bookworm
Architectures: arm64
Components: main
UDebComponents: main
Description: updated packages for mnt reform
${SIGNKEY:+SignWith: $SIGNKEY}

Codename: bookworm-backports
Label: bookworm-backports
Architectures: arm64
Components: main
UDebComponents: main
Description: updated packages for mnt reform
${SIGNKEY:+SignWith: $SIGNKEY}
END

REPREPRO_BASE_DIR="$(realpath reform-debian-packages/repo)"
export REPREPRO_BASE_DIR
reprepro export

##########
# temporarily backport packages from unstable to stable until these
# packages are in stable (probably with Trixie)
##########
cat << END > "reform-debian-packages/chdist/base/etc/apt/sources.list"
deb-src http://deb.debian.org/debian unstable main non-free
END
chdist --data-dir=reform-debian-packages/chdist apt-get base update
for p in reform-tools reform-branding reform-handbook; do
  # shellcheck disable=SC2016
  our_version=$(reprepro --list-format '${version}_${source}\n' -T deb listfilter "bookworm" "\$Source (== $p)" | sed 's/.*_.*(\(.*\))$/\1/;s/_.*//' | uniq)
  their_version=$(chdist --data-dir=reform-debian-packages/chdist apt-get base source --only-source -t "unstable" --no-act "$p" | sed "s/^Selected version '\\([^']*\\)' (unstable) for .*/\\1/;t;d")
  if test -z "$their_version"; then
    echo "E: cannot determine source version ofr $p" >&2
    exit 1
  fi
  if test -n "$our_version" && dpkg --compare-versions "$our_version" gt "$their_version"; then
    echo "I: package $p up to date" >&2
    continue
  fi
  reprepro removesrc bookworm "$p"
  tmpdir="$(mktemp -d)"
  trap 'rm -rf "$tmpdir"' EXIT HUP INT TERM
  CHDISTDATA="$(pwd)/reform-debian-packages/chdist"
  (
    cd "$tmpdir"
    chdist --data-dir="$CHDISTDATA" apt-get base source --only-source -t "unstable" --download-only "$p=$their_version"
    new_version="$their_version~bpo12+1"
    sbuild --dist=bookworm --chroot bookworm-arm64 --arch-all --arch-any \
      --build=arm64 --host=arm64 --no-clean-source --no-source-only-changes \
      --no-run-lintian --no-run-autopkgtest --no-apt-upgrade \
      --no-apt-distupgrade --verbose --profiles=nobiarch,nocheck,noudeb \
      --binNMU-changelog="$p ($new_version) bookworm; urgency=medium\n\n  * reform.debian.net rebuild for Debian Bookworm\n\n -- robot <reform@reform.repo>  $(date --date="@$SOURCE_DATE_EPOCH" --rfc-email)" \
      "${p}_$their_version.dsc"
    reprepro include bookworm "${p}_${new_version}_arm64.changes"
  )
  rm -rf "$tmpdir"
  trap "-" EXIT HUP INT TERM
done
###

cat << END > "reform-debian-packages/chdist/base/etc/apt/sources.list"
deb-src http://deb.debian.org/debian bookworm main
deb-src http://deb.debian.org/debian bookworm-updates main
deb-src http://security.debian.org/debian-security bookworm-security main
END
chdist --data-dir=reform-debian-packages/chdist apt-get base update

# Build packages

env --chdir=reform-debian-packages BASESUITE=bookworm OURSUITE=bookworm OURLABEL=reform.debian.net ./build_patched.sh
# sometimes, flash-kernel patches change (for example when adding a new SoM)
# and then flash-kernel has to be manually removed from the repo so that it
# gets rebuilt by running:
# $ REPREPRO_BASE_DIR=reform-debian-packages/repo reprepro removesrc bookworm flash-kernel
env --chdir=reform-debian-packages BASESUITE=bookworm OURSUITE=bookworm OURLABEL=reform.debian.net ./build_custom.sh

reprepro includedeb bookworm reform-debian-packages/fonts-reform-iosevka-term_2.3.0-1_all.deb
for c in reform-debian-packages/changes/*.changes; do
	[ -e "$c" ] || continue
	reprepro include bookworm "$c"
done

# Build Linux bookworm

# shellcheck disable=SC2016 # Intentional quoting technique
our_version=$(reprepro --list-format '${version}_${source}\n' -T deb listfilter "bookworm" "\$Source (== linux)" | sed 's/.*_.*(\(.*\))$/\1/;s/_.*//' | uniq)
their_version=$(chdist --data-dir=reform-debian-packages/chdist apt-get base source --only-source -t "bookworm" --no-act "linux" | sed "s/^Selected version '\\([^']*\\)' (bookworm) for linux/\\1/;t;d")
if [ -z "$their_version" ]; then
	echo "could not retrieve version of src:linux in bookworm" >&2
	exit 1
fi
if [ -z "$our_version" ] || dpkg --compare-versions "$our_version" lt "$their_version"; then
	tmpdir="$(mktemp -d)"
	trap 'rm -rf "$tmpdir"' EXIT HUP INT TERM
	env --chdir=reform-debian-packages/linux DEBEMAIL='robot <reform@reform.repo>' BUILD_ARCH=arm64 HOST_ARCH=arm64 BASESUITE=bookworm OURSUITE=bookworm OURLABEL=reform.debian.net VERSUFFIX=reform ROOTDIR=.. WORKDIR="$tmpdir" ./build.sh
	reprepro removesrc bookworm linux
	reprepro include bookworm reform-debian-packages/changes/linux.changes
	kver="$(dpkg-parsechangelog --show-field Version --file reform-debian-packages/linux/linux/debian/changelog)"
	dcmd rm "reform-debian-packages/linux/linux_${kver}.dsc"
	rm "reform-debian-packages/linux/$(readlink "reform-debian-packages/linux/linux_${kver}_arm64.build")"
	rm "reform-debian-packages/linux/linux_${kver}_arm64.build"
	rm -r reform-debian-packages/linux/linux/
	test ! -e "$tmpdir"
	trap "-" EXIT HUP INT TERM
fi

# Build Linux bookworm-backports

cat << END > "reform-debian-packages/chdist/base/etc/apt/sources.list"
deb-src http://deb.debian.org/debian bookworm main
deb-src http://deb.debian.org/debian bookworm-backports main
deb-src http://deb.debian.org/debian bookworm-updates main
deb-src http://security.debian.org/debian-security bookworm-security main
END
chdist --data-dir=reform-debian-packages/chdist apt-get base update
# shellcheck disable=SC2016 # Intentional quoting technique
our_version=$(reprepro --list-format '${version}_${source}\n' -T deb listfilter "bookworm-backports" "\$Source (== linux)" | sed 's/.*_.*(\(.*\))$/\1/;s/_.*//' | uniq)
their_version=$(chdist --data-dir=reform-debian-packages/chdist apt-get base source --only-source -t "bookworm-backports" --no-act "linux" | sed "s/^Selected version '\\([^']*\\)' (bookworm-backports) for linux/\\1/;t;d")
if [ -z "$their_version" ]; then
	echo "could not retrieve version of src:linux in bookworm-backports" >&2
	exit 1
fi
if [ -z "$our_version" ] || dpkg --compare-versions "$our_version" lt "$their_version"; then
	tmpdir="$(mktemp -d)"
	trap 'rm -rf "$tmpdir"' EXIT HUP INT TERM
	env --chdir=reform-debian-packages/linux DEBEMAIL='robot <reform@reform.repo>' BUILD_ARCH=arm64 HOST_ARCH=arm64 BASESUITE=bookworm-backports OURSUITE=bookworm-backports OURLABEL=reform.debian.net VERSUFFIX=reform ROOTDIR=.. WORKDIR="$tmpdir" ./build.sh
	reprepro removesrc bookworm-backports linux
	reprepro include bookworm-backports reform-debian-packages/changes/linux.changes
	kver="$(dpkg-parsechangelog --show-field Version --file reform-debian-packages/linux/linux/debian/changelog)"
	dcmd rm "reform-debian-packages/linux/linux_${kver}.dsc"
	rm "reform-debian-packages/linux/$(readlink "reform-debian-packages/linux/linux_${kver}_arm64.build")"
	rm "reform-debian-packages/linux/linux_${kver}_arm64.build"
	rm -r reform-debian-packages/linux/linux/
	test ! -e "$tmpdir"
	trap "-" EXIT HUP INT TERM
fi

# sync repositories so that reform-system-image/mkimage.sh can use them
rsync -Pha --delete reform-debian-packages/repo/ "$WWWDATA/debian/"

# The order of the system images in this string determines the order of the
# system images in the tables on the website.
SYSIMAGES="
reform-system-imx8mq
reform-system-ls1028a
reform-system-a311d
reform-system-imx8mp
reform-system-rk3588
pocket-reform-system-imx8mp
pocket-reform-system-a311d
pocket-reform-system-rk3588
reform-next-system-rk3588
"

bookworm_support() {
	case $1 in
		reform-system-imx8mq) return 0;;
		reform-system-ls1028a) return 0;;
		*) return 1;;
	esac
}

# System image

for DIST in bookworm bookworm-backports; do
	env --chdir=reform-system-image DIST=$DIST MIRROR=reform.debian.net ./mkimage.sh
	for PLATFORM in $SYSIMAGES; do
		if [ "$DIST" = "bookworm" ] && ! bookworm_support "$PLATFORM"; then
			continue
		fi
		SUFFIX=
		if [ "$DIST" = "bookworm-backports" ]; then
			SUFFIX="-bpo"
		fi
		gpg --armor --output="${PLATFORM}${SUFFIX}.img.xz.sig" --detach-sig ${SIGNKEY:+--default-key $SIGNKEY} "reform-system-image/$PLATFORM.img.xz"
		gpg --armor --output="${PLATFORM}${SUFFIX}.img.xz.bmap.sig" --detach-sig ${SIGNKEY:+--default-key $SIGNKEY} "reform-system-image/$PLATFORM.img.xz.bmap"
		mv "${PLATFORM}${SUFFIX}.img.xz.sig" "$WWWDATA/images"
		mv "${PLATFORM}${SUFFIX}.img.xz.bmap.sig" "$WWWDATA/images"
		mv "reform-system-image/$PLATFORM.img.xz" "$WWWDATA/images/${PLATFORM}${SUFFIX}.img.xz"
		mv "reform-system-image/$PLATFORM.img.xz.bmap" "$WWWDATA/images/${PLATFORM}${SUFFIX}.img.xz.bmap"
	done
done

# Debian Installer

for DIST in bookworm bookworm-backports; do
	for PLATFORM in $SYSIMAGES; do
		if [ "$DIST" = "bookworm" ] && ! bookworm_support "$PLATFORM"; then
			continue
		fi
		SUFFIX=
		if [ "$DIST" = "bookworm-backports" ]; then
			SUFFIX="-bpo"
		fi
		env --chdir=reform-debian-installer ./run.sh -d "$DIST" -m reform.debian.net "$PLATFORM"
		mv reform-debian-installer/partition.img.bmap reform-debian-installer/partition.img.xz.bmap
		xz --force --verbose -9 --extreme reform-debian-installer/partition.img
		gpg --armor --output="${PLATFORM}${SUFFIX}.img.xz.sig" --detach-sig ${SIGNKEY:+--default-key $SIGNKEY} reform-debian-installer/partition.img.xz
		gpg --armor --output="${PLATFORM}${SUFFIX}.img.xz.bmap.sig" --detach-sig ${SIGNKEY:+--default-key $SIGNKEY} reform-debian-installer/partition.img.xz.bmap
		mv "${PLATFORM}${SUFFIX}.img.xz.sig" "$WWWDATA/d-i"
		mv "${PLATFORM}${SUFFIX}.img.xz.bmap.sig" "$WWWDATA/d-i/"
		mv reform-debian-installer/partition.img.xz "$WWWDATA/d-i/${PLATFORM}${SUFFIX}.img.xz"
		mv reform-debian-installer/partition.img.xz.bmap "$WWWDATA/d-i/${PLATFORM}${SUFFIX}.img.xz.bmap"
	done
done

# Website

rm -f machines/*.conf
[ -d machines ] && rmdir machines
dpkg-deb --fsys-tarfile reform-debian-packages/repo/pool/main/r/reform-tools/reform-tools_1.*_all.deb \
	| tar --strip-components=4 -x ./usr/share/reform-tools/machines/

mkdir -p reform.debian.net/_data
for TYPE in "images" "d-i"; do
	for PLATFORM in $SYSIMAGES; do
		echo " - sysimage: $PLATFORM"
		echo "   name: $(basename "$(grep -l "^SYSIMAGE=\"$PLATFORM\"\$" machines/*.conf)" .conf)"
		if bookworm_support "$PLATFORM"; then
			echo "   bookworm: $(numfmt --to=si "$(stat -c %s "/var/www/$TYPE/$PLATFORM.img.xz")")"
		fi
		echo "   bookworm-bpo: $(numfmt --to=si "$(stat -c %s "/var/www/$TYPE/$PLATFORM-bpo.img.xz")")"
	done > "reform.debian.net/_data/${TYPE}.yaml"
done

env --chdir=reform.debian.net jekyll build

cp -a reform.debian.net/_site/* "$WWWDATA/"

if [ -z "$SIGNKEY" ]; then
	echo "no key was provided, so even though the script was successful,"
	echo "treat it as a failure to disallow unsigned repos"
	exit 1
fi

echo SUCCESS

